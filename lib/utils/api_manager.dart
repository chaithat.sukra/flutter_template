
import 'package:auto_route/annotations.dart';
import 'package:dio/dio.dart';
import 'package:flutter_template/business_logic/models/core/response_generic_data.dart';
import 'package:retrofit/retrofit.dart';

import '../config.dart';

part 'api_manager.g.dart';

@RestApi(baseUrl: Config.kURL_API)
abstract class ApiManager {
  factory ApiManager(Dio dio) {
    dio.options = BaseOptions(
        receiveTimeout: 30 * 1000,
        connectTimeout: 30 * 1000,
        contentType: 'application/json');
    return _ApiManager(dio);
  }

  @PUT(Apis.kAUTH)
  Future<ResponseGenericData> login(@Body() Map<String, dynamic> body);

  @GET(Apis.kGET_PROFILE)
  Future<ResponseGenericData> getProfile(@Query("username") String username);
}

class Apis {
  static const String kAUTH = '/auth/v1/authentication';
  static const String kGET_PROFILE = '/auth/v1/profiles';
}
