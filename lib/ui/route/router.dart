import 'package:auto_route/auto_route.dart';
import 'package:flutter_template/ui/pages/login_page.dart';
import 'package:flutter_template/ui/pages/profile_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: LoginPage, initial: true),
    AutoRoute(
      path: '/profile:username',
      page: ProfilePage,
      children: [
        RedirectRoute(path: '*', redirectTo: ''),
      ],
    ),
  ],
)
class $AppRouter {}
