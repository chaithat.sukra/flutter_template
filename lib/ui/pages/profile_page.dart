
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/business_logic/login_viewmodel.dart';
import 'package:flutter_template/business_logic/profile_viewmodel.dart';
import 'package:flutter_template/injection.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key, this.username}) : super(key: key);

  final String? username;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>{

  SizedBox spacing = const SizedBox(height: 8);

  @override
  void initState() {
    super.initState();

    context.read<ProfileViewModel>().getProfile(widget.username ?? "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("Login"),
              spacing,
              Text(context.watch<ProfileViewModel>().profile == null ? "Loading.." : "Username: ${context.watch<ProfileViewModel>().profile?.username}"),
              spacing,
              Text("Password: ${context.watch<ProfileViewModel>().profile?.password}"),
              spacing,
            ],
          )
      ),
    );
  }
}
