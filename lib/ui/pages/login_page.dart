
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/business_logic/login_viewmodel.dart';
import 'package:flutter_template/ui/route/router.gr.dart';
import 'package:provider/provider.dart';
import 'package:auto_route/auto_route.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  SizedBox spacing = const SizedBox(height: 8);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Login"),
              spacing,
              SizedBox(
                width: 200,
                height: 44,
                child: TextFormField(
                  controller: _usernameController,
                  decoration: const InputDecoration(border: OutlineInputBorder()),
                ),
              ),
              spacing,
              SizedBox(
                width: 200,
                height: 44,
                child: TextFormField(
                  controller: _passController,
                  decoration: const InputDecoration(border: OutlineInputBorder()),
                ),
              ),
              spacing,
              Consumer<LoginViewModel>(
                builder: (context, model, _) {
                  String title = "Sign in With Email and password";
                  return ElevatedButton(
                      child: Text(title),
                      onPressed: () => model.login(_usernameController.text, _passController.text).then((isSuccess) => {
                        context.router.push(ProfileRoute(username: _usernameController.text))
                      }).catchError((e) => {
                        print(e)
                      })
                  );
                },
              )
            ],
          )
      ),
    );
  }
}
