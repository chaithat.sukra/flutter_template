
import 'package:json_annotation/json_annotation.dart';

part 'profile.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Profile {
  int id;
  String username;
  String password;
  String firstName;
  String lastName;
  String fullName;
  String fullNameTh;
  String title;
  bool isActive;
  String thaiId;
  String mobilePhone;
  String accountId;
  String dof;
  String placeOfBirth;
  int roleId;
  int organizationId;
  DateTime? createdDate;

  Profile(
      {required this.id,
        required this.username,
        required this.password,
        required this.firstName,
        required this.lastName,
        required this.fullName,
        required this.fullNameTh,
        required this.title,
        required this.isActive,
        required this.thaiId,
        required this.mobilePhone,
        required this.accountId,
        required this.dof,
        required this.placeOfBirth,
        required this.roleId,
        required this.organizationId,
        required this.createdDate,
      });

  static Profile fromJson(Map<String, dynamic> json) => _$ProfileFromJson(json);
}
