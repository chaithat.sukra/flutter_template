// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profile _$ProfileFromJson(Map<String, dynamic> json) => Profile(
      id: json['id'] as int,
      username: json['username'] as String,
      password: json['password'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      fullName: json['full_name'] as String,
      fullNameTh: json['full_name_th'] as String,
      title: json['title'] as String,
      isActive: json['is_active'] as bool,
      thaiId: json['thai_id'] as String,
      mobilePhone: json['mobile_phone'] as String,
      accountId: json['account_id'] as String,
      dof: json['dof'] as String,
      placeOfBirth: json['place_of_birth'] as String,
      roleId: json['role_id'] as int,
      organizationId: json['organization_id'] as int,
      createdDate: json['created_date'] == null
          ? null
          : DateTime.parse(json['created_date'] as String),
    );
