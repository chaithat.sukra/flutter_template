
import 'package:json_annotation/json_annotation.dart';

import 'profile.dart';

part 'auth.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Auth {
  String token;
  Profile profile;

  Auth(
      {required this.token,
        required this.profile});

  static Auth fromJson(Map<String, dynamic> json) => _$AuthFromJson(json);
}
