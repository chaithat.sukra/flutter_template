

import 'package:flutter/cupertino.dart';
import 'package:flutter_template/injection.dart';
import 'package:flutter_template/services/auth_service.dart';
import 'package:injectable/injectable.dart';

import 'models/auth.dart';

@injectable
class LoginViewModel extends ChangeNotifier {

  Auth? _auth;
  Auth? get auth => _auth;

  Future<bool> login(String username, String password) async {
    _auth = await getIt<AuthService>().login(username, password);
    return true;
  }
}
