

import 'package:flutter/cupertino.dart';
import 'package:flutter_template/business_logic/models/profile.dart';
import 'package:flutter_template/injection.dart';
import 'package:flutter_template/services/auth_service.dart';
import 'package:injectable/injectable.dart';

import 'models/auth.dart';

@injectable
class ProfileViewModel extends ChangeNotifier {

  Profile? _profile;
  Profile? get profile => _profile;

  Future<void> getProfile(String username) async {
    _profile = await getIt<AuthService>().getProfile(username);
    notifyListeners();
  }
}
