

import 'package:flutter_template/business_logic/models/auth.dart';
import 'package:flutter_template/business_logic/models/profile.dart';

abstract class IApi {

  Future<Auth> login(String username, String password);
  Future<Profile> getProfile(String username);
}