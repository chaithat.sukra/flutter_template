
import 'package:dio/dio.dart';
import 'package:flutter_template/business_logic/models/auth.dart';
import 'package:flutter_template/business_logic/models/profile.dart';
import 'package:flutter_template/share/exceptions/fetch_data_exception.dart';
import 'package:flutter_template/utils/api_manager.dart';
import 'package:injectable/injectable.dart';

import 'interfaces/i_api.dart';

@singleton
class AuthService implements IApi {

  late Dio dio;
  late ApiManager apiManager;

  AuthService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Auth> login(String username, String password) async {
    var body = {
      'username': username,
      'password': password,
      "organization_id": 3
    };

    try {
      final res = await apiManager.login(body);
      return Auth.fromJson(res.data);
    } catch (e) {
      throw FetchDataException("Invalid Request");
    }
  }

  @override
  Future<Profile> getProfile(String username) async {
    try {
      final res = await apiManager.getProfile(username);
      return Profile.fromJson(res.data);
    } catch (e) {
      throw FetchDataException("Invalid Request");
    }
  }
}
