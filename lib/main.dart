
import 'package:flutter/material.dart';
import 'package:flutter_template/business_logic/login_viewmodel.dart';
import 'package:flutter_template/injection.dart';
import 'package:flutter_template/ui/route/router.gr.dart';
import 'package:injectable/injectable.dart';
import 'package:provider/provider.dart';

import 'business_logic/profile_viewmodel.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureInjection(Environment.prod);
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => getIt<LoginViewModel>()),
        ChangeNotifierProvider(create: (_) => getIt<ProfileViewModel>()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final AppRouter _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate:_appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
